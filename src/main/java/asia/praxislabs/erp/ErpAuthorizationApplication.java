package asia.praxislabs.erp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErpAuthorizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErpAuthorizationApplication.class, args);
	}
}
