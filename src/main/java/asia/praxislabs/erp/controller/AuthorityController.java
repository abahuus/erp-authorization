/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.controller;

import asia.praxislabs.erp.domain.Authority;
import asia.praxislabs.erp.repository.AuthorityRepository;
import asia.praxislabs.erp.service.AuthorityService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author User-pc
 */

@RestController
@RequestMapping("/authority")
public class AuthorityController {
    
    @Autowired
    private AuthorityService authorityService;
    
    @Autowired
    private AuthorityRepository authorityRepository;
    
    
    @GetMapping("")
    public Page<Authority> getAll(Pageable pageable) {
        return authorityService.getAll(pageable);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Authority> getId(@PathVariable("id") String id) {
        return new ResponseEntity<>(authorityService.getId(id), HttpStatus.OK);
    }
    
    @PostMapping("")
    public ResponseEntity<Authority> save(@RequestBody @Valid Authority authority) {
        Authority a = authorityService.save(authority);
        if(a == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
        }
            return new ResponseEntity<>(a, HttpStatus.CREATED);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<Authority> update(@RequestBody @Valid Authority authority,
           @PathVariable("id") String id) {
        Authority u = authorityService.save(authority);
        if(u == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
        }
            return new ResponseEntity<>(u, HttpStatus.CREATED);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity delete(
        @PathVariable("id") String id
    ) {
        try {
            authorityRepository.deleteById(id);
        return new ResponseEntity<> (null, HttpStatus.CREATED);
        } catch (Exception e) {
        return new ResponseEntity<> (null, HttpStatus.NOT_MODIFIED);    
        }
        
    }
    
}
