/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.controller;

import asia.praxislabs.erp.domain.master.Customer;
import asia.praxislabs.erp.repository.CustomerRepository;
import asia.praxislabs.erp.service.CustomerService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author User-pc
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {
    
    @Autowired
    private CustomerService customerService;
    
    
        
    @GetMapping("")
    public Page<Customer> getAll(Pageable pageable) {
        return customerService.getAll(pageable);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Customer> getId(@PathVariable("id") String id) {
        return new ResponseEntity<>(customerService.getId(id), HttpStatus.OK);
    }
    
    @PostMapping("/")
    public ResponseEntity<Customer> save(@RequestBody @Valid Customer customer) {
        Customer u = customerService.save(customer);
        if(u == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
        }
            return new ResponseEntity<>(null, HttpStatus.CREATED);
    }
    
}
