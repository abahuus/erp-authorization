/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author User-pc
 
@RestController
public class HelloController {
    @RequestMapping("/")
    String home() {
    return "Hello World";
    } 
  */  
@RestController
@RequestMapping("/api")
public class HelloController {
    
    @GetMapping("/get")
    String home() {
    return "Hello World ! 11";
    }    
    
    @PostMapping("/post")
    String save() {
    return "Hello World !12";
    }
    
    @PutMapping("/put")
    String update() {
    return "Hello World !13";
    }
    
    @DeleteMapping("/delete")
    String delete() {
    return "Hello World !14";
    }
}
