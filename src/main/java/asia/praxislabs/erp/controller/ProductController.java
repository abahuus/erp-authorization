/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.controller;

import asia.praxislabs.erp.domain.master.Product;
import asia.praxislabs.erp.repository.ProductRepository;
import asia.praxislabs.erp.service.ProductService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author User-pc
 */

@RestController
@RequestMapping("/product")
public class ProductController {
    
    @Autowired
    private ProductService productService;
    
    @Autowired
    private ProductRepository productRepository;
    
     @GetMapping("")
    public Page<Product> getAll(Pageable pageable) {
        return productService.getAll(pageable);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Product> getId(@PathVariable("id") String id) {
        return new ResponseEntity<>(productService.getId(id), HttpStatus.OK);
    }
    
    
    @PostMapping("")
    public ResponseEntity<Product> save(@RequestBody @Valid Product product) {
        Product u = productService.save(product);
        if(u == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
        }
            return new ResponseEntity<>(u, HttpStatus.CREATED);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@RequestBody @Valid Product product,
           @PathVariable("id") String id) {
        Product p = productService.save(product);
        if(p == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
        }
            return new ResponseEntity<>(p, HttpStatus.CREATED);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Product> delete(@PathVariable("id") String id) {
        productService.delete(id);
        return new ResponseEntity<> (productService.getId(id),HttpStatus.OK);
    }
        
    
            
//    ) {
//        try {
//            productRepository.deleteById(id);
//        return new ResponseEntity<> (null, HttpStatus.CREATED);
//        } catch (Exception e) {
//        return new ResponseEntity<> (null, HttpStatus.NOT_MODIFIED);    
//        }
//        
//    }
       
    
}
