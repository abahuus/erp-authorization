/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.controller;

import asia.praxislabs.erp.domain.sales.Sales;
import asia.praxislabs.erp.repository.SalesRepository;
import asia.praxislabs.erp.service.SalesService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author User-pc
 */
@RestController
@RequestMapping("/sales")
public class SalesController {
    
    @Autowired
    private SalesService salesService;
    
    @Autowired
    private SalesRepository salesRepository;
    
    
    @GetMapping("")
    public Page<Sales> getAll(Pageable pageable) {
        return salesService.getAll(pageable);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Sales> getId(@PathVariable("id") String id) {
        return new ResponseEntity<>(salesService.getId(id), HttpStatus.OK);
    }
    
    @PostMapping("/")
    public ResponseEntity<Sales> save(@RequestBody @Valid Sales sales) {
        Sales sls = salesService.save(sales);
        if(sls == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
        }
            return new ResponseEntity<>(null, HttpStatus.CREATED);
    }
    
}
