/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.controller;

import asia.praxislabs.erp.domain.Users;
import asia.praxislabs.erp.service.UsersService;
import asia.praxislabs.erp.repository.UsersRepository;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author User-pc
 
@RestController
public class HelloController {
    @RequestMapping("/")
    String home() {
    return "Hello World";
    } 
  */  
@RestController
@RequestMapping("/users")
public class UsersController {
    
    @Autowired
    private UsersService usersService;
    
    @Autowired
    private UsersRepository usersRepository;
    
        
    @GetMapping("")
    public Page<Users> getAll(Pageable pageable) {
        return usersService.getAll(pageable);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<Users> getId(@PathVariable("id") String id) {
        return new ResponseEntity<>(usersService.getId(id), HttpStatus.OK);
    }
    
    @PostMapping("")
    public ResponseEntity<Users> save(@RequestBody @Valid Users users) {
        Users u = usersService.save(users);
        if(u == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
        }
            return new ResponseEntity<>(u, HttpStatus.CREATED);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<Users> update(@RequestBody @Valid Users users,
           @PathVariable("id") String id) {
        Users u = usersService.save(users);
        if(u == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
        }
            return new ResponseEntity<>(u, HttpStatus.CREATED);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity delete(
        @PathVariable("id") String id
    ) {
        try {
            usersRepository.deleteById(id);
        return new ResponseEntity<> (null, HttpStatus.CREATED);
        } catch (Exception e) {
        return new ResponseEntity<> (null, HttpStatus.NOT_MODIFIED);    
        }
        
    }
    
//    @PutMapping("update/{id}")
//    public ResponseEntity<Object> update(@RequestBody Users users, @PathVariable String id) {
//
//	Optional<Users> usersOptional = usersService.update(users);
//
//	if (!usersOptional.isPresent())
//		return ResponseEntity.notFound().build();
//
//	users.setId(id);
//	
//	usersService.save(users);
//
//	return ResponseEntity.noContent().build();
//}
    
//    @PutMapping("update/{id}")
//    public ResponseEntity<Users> update(@RequestBody Users users ,@PathVariable("id") String id) {
//
//	Users us = usersService.getId(id);
//
//	if (us == null) {
//            return new ResponseEntity(null,HttpStatus.NOT_FOUND);
//    }
//
//	us.setUsername(users.getUsername());
//        us.setPassword(users.getPassword());
//        us.setEnabled(users.getEnabled());
//	
//	usersService.save(us);
//
//	return new ResponseEntity<Users> (us,HttpStatus.OK);
//}
    
//    @PostMapping("/save")
//    public ResponseEntity<Users> save(
//        @RequestB)
    
//    @GetMapping("/get")
//    String home() {
//    return "GET";
//    }    
//    
//    @PostMapping("/post")
//    String save() {
//    return "POST";
//    }
//    
//    @PutMapping("/put")
//    String update() {
//    return "UPDATE";
//    }
//    
//    @DeleteMapping("/delete")
//    String delete() {
//    return "DELETE";
//    }
}
