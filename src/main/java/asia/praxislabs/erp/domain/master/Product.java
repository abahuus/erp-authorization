/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.domain.master;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author User-pc
 */

@Data
@Entity
@Table(name = "product")
public class Product implements Serializable {
    
    @Id
    @GeneratedValue(generator ="uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    
   // @Id
   // @GeneratedValue(strategy = GenerationType.AUTO)
   // private Integer id;
    
    @Column(name = "code", unique = true, nullable = false, length = 35)
    private String code;
    
    @Column(name = "nama_barang", unique = false, nullable = false, length = 99)
    private String namaBarang;
    
    //Harga Jual
    @Column(name = "harga", unique = false, nullable = false, precision = 11, scale = 0)
    private BigDecimal harga;
    
    
    
    
}
