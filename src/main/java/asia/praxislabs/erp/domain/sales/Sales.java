/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.domain.sales;

import asia.praxislabs.erp.domain.master.Customer;
import asia.praxislabs.erp.domain.master.Employee;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author User-pc
 */

@Data
@Entity
@Table(name = "sales")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sales implements Serializable {
    
    @Id
    @GeneratedValue(generator ="uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id", unique = true, nullable = false, length = 55)
    private String id;
    
    @Column(name = "no_faktur", unique = true, nullable = false, length = 15)
    private String noFaktur;
    
    @ManyToOne
    @JoinColumn(name = "id_customer", unique = false, nullable = true)
    private Customer customer; 
    
    @ManyToOne
    @JoinColumn(name = "id_employee", unique = false, nullable = true)
    private Employee employee;  
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sales_date", unique = false, nullable = false)
    private Date salesDate;
    
    //total  penjualan harga barang setelah potong diskon
    @Column(name = "total_sales", unique = false, nullable = false, precision = 15, scale = 0)
    private BigDecimal totalSales;
    
    //harus dikasih : orphanRemoval = true, agar bisa menghapus salah satu : salesDetails
    @OneToMany(mappedBy = "sales", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    /*DISAMPING DIPERLUKAN UNTUK INPUT DATA MASTERDETAIL */
    private List<SalesDetails> salesDetails;
    
}
