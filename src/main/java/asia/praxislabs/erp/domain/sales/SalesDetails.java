/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.domain.sales;

import asia.praxislabs.erp.domain.master.Product;
import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author User-pc
 */
@Data
@Entity
@Table(name = "sales_detail")
public class SalesDetails implements Serializable {
    
    @Id
    @GeneratedValue(generator ="uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id", unique = true, nullable = false, length = 55)
    private String id;
    
    @ManyToOne
    @JoinColumn(name = "id_product", unique = false, nullable = true)
    private Product product;
    
    //harga jual
    @Column(name = "harga", unique = false, nullable = false, precision = 15, scale = 0)
    private BigDecimal harga;
    
    @Column(name = "quantity", unique = false, nullable = false, precision = 9, scale = 2)
    private BigDecimal quantity;
    
    //totalPrice diambil dari : sellingPrice*quantity
    @Column(name = "sub_total", unique = false, nullable = true, precision = 19, scale = 0)
    private BigDecimal subTotal;
    
    @ManyToOne
    @JoinColumn(name = "id_sales", unique = false, nullable = false, updatable = true)
    @JsonBackReference
    /*DISAMPING DIPERLUKAN UNTUK INPUT DATA MASTERDETAIL*/
    private Sales sales;
    
}
