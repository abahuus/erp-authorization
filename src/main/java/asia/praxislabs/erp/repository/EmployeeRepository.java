/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.repository;

import asia.praxislabs.erp.domain.master.Customer;
import asia.praxislabs.erp.domain.master.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author User-pc
 */
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, String>{
    
   @Query("SELECT c FROM Employee c WHERE c.id=:id") 
   Employee getId(@Param("id") String id);
}
