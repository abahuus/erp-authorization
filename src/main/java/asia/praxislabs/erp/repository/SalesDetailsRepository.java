/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.repository;

import asia.praxislabs.erp.domain.sales.SalesDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author User-pc
 */
public interface SalesDetailsRepository extends PagingAndSortingRepository<SalesDetails, String> {
    
   @Query("SELECT s FROM SalesDetails s WHERE s.id=:id") 
   SalesDetails getId(@Param("id") String id);
    
}
