/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.repository;

import asia.praxislabs.erp.domain.sales.Sales;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author User-pc
 */
public interface SalesRepository extends PagingAndSortingRepository<Sales, String> {
   @Query("SELECT s FROM Sales s WHERE s.id=:id") 
   Sales getId(@Param("id") String id);
    
}
