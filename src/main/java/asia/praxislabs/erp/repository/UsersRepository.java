/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.repository;

import asia.praxislabs.erp.domain.Users;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author User-pc
 */

public interface UsersRepository extends PagingAndSortingRepository<Users, String> { //String mengacu pd jenis data
   @Query("SELECT u FROM Users u WHERE u.id=:id") 
   Users getId(@Param("id") String id);
}
