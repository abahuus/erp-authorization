/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service;

import asia.praxislabs.erp.domain.Authority;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author User-pc
 */
public interface AuthorityService {
    
    Page<Authority> getAll(Pageable pageable);
    Authority getId(String id);
    Authority save(Authority authority);
    Authority update(Authority authority);
    void delete(String id);
    List<Authority> getAuthority(String authority);
    
}
