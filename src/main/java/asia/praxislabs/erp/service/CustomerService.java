/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service;

import asia.praxislabs.erp.domain.master.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 *
 * @author User-pc
 */
public interface CustomerService { 
    
    Page<Customer> getAll(Pageable pageable);
    Customer getId(String id);
    Customer save(Customer customer);
    Customer update(Customer customer);
    void delete(String id);
    //List<Users> getUsername(String username);
    
}
