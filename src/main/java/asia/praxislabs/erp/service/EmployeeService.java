/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service;

import asia.praxislabs.erp.domain.master.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author User-pc
 */
public interface EmployeeService {  
    
    Page<Employee> getAll(Pageable pageable);
    Employee getId(String id);
    Employee save(Employee employee);
    Employee update(Employee employee);
    void delete(String id);
    //List<Employee> getUsername(String username);
    
    
}
