/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service;

import asia.praxislabs.erp.domain.master.Product;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author User-pc
 */
public interface ProductService {
    
    Page<Product> getAll(Pageable pageable);
    Product getId(String id);
    Product save(Product product);
    Product update(Product product);
    void delete(String id);
    List<Product> getNama_Barang(String nama_Barang);
    
}
