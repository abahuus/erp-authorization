/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service;

import asia.praxislabs.erp.domain.sales.SalesDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 *
 * @author User-pc
 */
public interface SalesDetailsService {
    
    Page<SalesDetails> getAll(Pageable pageable);
    SalesDetails getId(String id);
    SalesDetails save(SalesDetails salesDetails);
    SalesDetails update(SalesDetails salesDetails);
    void delete(String id);
    
}
