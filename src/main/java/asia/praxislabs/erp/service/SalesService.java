/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service;

import asia.praxislabs.erp.domain.sales.Sales;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author User-pc
 */
public interface SalesService {
    
    Page<Sales> getAll(Pageable pageable);
    Sales getId(String id);
    Sales save(Sales sales);
    Sales update(Sales sales);
    void delete(String id);
    //List<Sales> getNama_Barang(String nama_Barang);
    
}
