/*
 * SEMUA HANYA MILIK ALLAH
 * MAHA SUCI ALLAH, SEGALA PUJI BAGI ALLAH, TIDAK ADA TUHAN SELAIN ALLAH, MAHA BESAR ALLAH
 * TIDAK ADA DAYA DAN UPAYA KECUALI DENGAN PERTOLONGAN ALLAH
 */
package asia.praxislabs.erp.service;

import asia.praxislabs.erp.domain.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 *
 * @author jp
 */
public interface UsersService {

    Page<Users> getAll(Pageable pageable);
    Users getId(String id);
    Users save(Users users);
    Users update(Users users);
    void delete(String id);
    List<Users> getUsername(String username);

}
