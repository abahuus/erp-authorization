/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service.impl;

import asia.praxislabs.erp.domain.Authority;
import asia.praxislabs.erp.repository.AuthorityRepository;
import asia.praxislabs.erp.service.AuthorityService;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author User-pc
 */
@Component("authorityService")
@Transactional
public class AuthorityServiceImpl implements AuthorityService {

    private final AuthorityRepository authorityRepository;

    public AuthorityServiceImpl(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    @Override
    public Page<Authority> getAll(Pageable pageable) {
        return authorityRepository.findAll(pageable);
    }

    @Override
    public Authority getId(String id) {
        return authorityRepository.getId(id);
    }

    @Override
    public Authority save(Authority authority) {

        return authorityRepository.save(authority);

    }

    @Override
    public Authority update(Authority authority) {

        return authorityRepository.save(authority);

    }

    @Override
    public void delete(String id) {

        authorityRepository.deleteById(id);

    }

    @Override
    public List<Authority> getAuthority(String authority) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
