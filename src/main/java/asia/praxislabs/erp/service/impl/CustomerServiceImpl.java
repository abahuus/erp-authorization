/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service.impl;

import asia.praxislabs.erp.domain.master.Customer;
import asia.praxislabs.erp.repository.CustomerRepository;
import asia.praxislabs.erp.service.CustomerService;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author User-pc
 */
@Component("customerService")
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Page<Customer> getAll(Pageable pageable) {
        return customerRepository .findAll(pageable);
    }

    @Override
    public Customer getId(String id) {
        return customerRepository.getId(id);
    }

    @Override
    public Customer save(Customer customer) {
        
        return customerRepository.save(customer);
        
    }

    @Override
    public Customer update(Customer customer) {
        return customerRepository.save(customer);
        
    }

    @Override
    public void delete(String id) {
        
        customerRepository.deleteById(id);
    }


}
