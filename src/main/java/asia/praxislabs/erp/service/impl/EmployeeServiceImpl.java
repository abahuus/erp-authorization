/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service.impl;

import asia.praxislabs.erp.domain.master.Employee;
import asia.praxislabs.erp.repository.EmployeeRepository;
import asia.praxislabs.erp.service.CustomerService;
import asia.praxislabs.erp.service.EmployeeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author User-pc
 */
@Component("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Page<Employee> getAll(Pageable pageable) {

        return employeeRepository.findAll(pageable);
    }

    @Override
    public Employee getId(String id) {
        return employeeRepository.getId(id);
    }

    @Override
    public Employee save(Employee employee) {

        return employeeRepository.save(employee);

    }

    @Override
    public Employee update(Employee employee) {
        return employeeRepository.save(employee);

    }

    @Override
    public void delete(String id) {
        employeeRepository.deleteById(id);

    }

}
