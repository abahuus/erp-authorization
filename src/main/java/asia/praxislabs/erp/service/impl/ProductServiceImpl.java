/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service.impl;

import asia.praxislabs.erp.domain.master.Product;
import asia.praxislabs.erp.repository.ProductRepository;
import asia.praxislabs.erp.service.ProductService;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author User-pc
 */

@Component("productService")
@Transactional
public class ProductServiceImpl implements ProductService {
    
    private final ProductRepository productRepository;
    
    public ProductServiceImpl (ProductRepository productRepository)
    {
            this.productRepository = productRepository;
    }


    @Override
    public Page<Product> getAll(Pageable pageable) {
        return productRepository .findAll(pageable);
    }

    @Override
    public Product getId(String id) {
        return productRepository.getId(id);
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product update(Product product) {
        return productRepository.save(product);
   }

    @Override
    public void delete(String id) {
        productRepository.deleteById(id);
    }

    @Override
    public List<Product> getNama_Barang(String nama_Barang) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
