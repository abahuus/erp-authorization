/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service.impl;

import asia.praxislabs.erp.domain.sales.Sales;
import asia.praxislabs.erp.repository.SalesRepository;
import asia.praxislabs.erp.service.SalesService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author User-pc
 */
@Component("salesService")
@Transactional
public class SalesServiceImpl implements SalesService {
    
    private final SalesRepository salesRepository;
    

    public SalesServiceImpl(SalesRepository salesRepository) {
        this.salesRepository = salesRepository;
    }

    @Override
    public Page<Sales> getAll(Pageable pageable) {
        return salesRepository.findAll(pageable);
    }

    @Override
    public Sales getId(String id) {
         return salesRepository.getId(id);
    }

    @Override
    public Sales save(Sales sales) {
        return salesRepository.save(sales);
    }

    @Override
    public Sales update(Sales sales) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
