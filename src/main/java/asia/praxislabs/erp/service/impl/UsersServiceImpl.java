/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asia.praxislabs.erp.service.impl;


import asia.praxislabs.erp.domain.Users;
import asia.praxislabs.erp.repository.UsersRepository;
import asia.praxislabs.erp.service.UsersService;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author User-pc
 */
@Component("usersService")
@Transactional
public class UsersServiceImpl implements UsersService{
    
    private final UsersRepository usersRepository;
    
    public UsersServiceImpl (UsersRepository usersRepository)
    {
        this.usersRepository = usersRepository;
    }
    
 

    @Override
    public Page<Users> getAll(Pageable pageable) {
        return usersRepository .findAll(pageable);
    }

    @Override
    public Users getId(String id) {
        return usersRepository.getId(id);
    }

    @Override
    public Users save(Users users) {
        return usersRepository.save(users);
    }

    @Override
    public Users update(Users users) {
        return usersRepository.save(users);
    }

    @Override
    public void delete(String id) {
        usersRepository.deleteById(id);
    }

    @Override
    public List<Users> getUsername(String username) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
