/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  User-pc
 * Created: Dec 18, 2018
 */

SELECT trans_voucher_batch.doc_ref,
trans_voucher_batch.flag_approved, 
tbl_partner.partner_name,
tbl_regional.rg_name AS city, 
trans_voucher_batch.date_trans,
trans_voucher_batch.partner_id AS customer_id, 
trans_voucher_batch.debet,
trans_voucher_batch.date_trans + '1 day'::interval day * trans_voucher_batch.term_days::double precision AS due_date, 
trans_voucher_batch.type_trans,
trans_voucher_batch.term_days,
tbl_term_payment.reminder_days,
(trans_voucher_batch.date_trans + '1 day'::interval day * trans_voucher_batch.term_days::double precision)::timestamp with time zone - now() AS payment_lead_time 
FROM finance.trans_voucher_batch 
JOIN finance.tbl_partner ON trans_voucher_batch.partner_id::text = tbl_partner.partner_id::text
JOIN finance.trans_code_line ON trans_voucher_batch.type_trans::text = trans_code_line.type_id::text
JOIN finance.tbl_term_payment ON trans_voucher_batch.term_pay_code::text = tbl_term_payment.code::text
JOIN finance.tbl_regional ON tbl_partner.city::text = tbl_regional.rg::text
GROUP BY trans_voucher_batch.partner_id,
tbl_partner.partner_name, 
trans_voucher_batch.type_trans,
trans_code_line.type_desc, 
tbl_term_payment.reminder_days,
trans_voucher_batch.term_days, 
tbl_regional.rg_name,
trans_voucher_batch.flag_approved, 
trans_voucher_batch.debet,
trans_voucher_batch.credit, 
trans_voucher_batch.doc_ref,
trans_voucher_batch.date_trans 
HAVING trans_voucher_batch.type_trans::bpchar IN ('CAP'::bpchar,'C-APDP'::bpchar, 'C-SALDOAP'::bpchar,'RPURCH'::bpchar)
ORDER BY ((trans_voucher_batch.date_trans + '1 day'::interval day * trans_voucher_batch.term_days::double precision)::timestamp with time zone - now())DESC;


