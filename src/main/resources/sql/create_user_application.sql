--------------- Create Table ---------------
CREATE TABLE authority (
	id varchar(55),
	authority varchar(25),
   id_user varchar(25),
	primary key (id)
);

CREATE TABLE users (
	id varchar(55),
	code varchar(25),
	enabled boolean not null,
	username varchar(25) not null,
	password varchar(255) not null,
	primary key (id)
);