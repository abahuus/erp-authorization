/* 
 * لا اله الا الله محمد الرسول الله
 */
/**
 * Author:  jp
 * Created: Mar 7, 2019
 */

CREATE USER 'erp'@'localhost' IDENTIFIED BY 'erp123';
GRANT ALL ON erp_praxislabs.* TO 'erp'@'localhost';
FLUSH PRIVILEGES;
SHOW GRANTS FOR erp@localhost;
